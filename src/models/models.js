import mongoose from 'mongoose';

const Scema = mongoose.Schema;

export const AuthSchema = new Schema({
    name:{
        type:String
    },

    email:{
        type:String
    },

    password:{
        type:String
    },

    timestamp:{
        type:Date,
        default:Date.now
    }
})