import express from 'express';
import routes from './src/routes/routes';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

const app = express();
const PORT = 9000;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb+srv://user:4313Samadhi@cluster0-jqb4b.mongodb.net/test?retryWrites=true&w=majority',{
    useNewUrlParser:true,
    useUnifiedTopology:true
});

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

routes(app)

app.get('/',(req,res)=>
    res.send('Node and express serever running on port and CI/CD test 1 '+PORT)
);

app.listen(PORT,()=>
    console.log('Your server is running on port '+PORT)  
);